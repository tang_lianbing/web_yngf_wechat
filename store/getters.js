const getters = {
  token: state => state.user.token,
  accessToken:state => state.user.accessToken,
  loginInfo: state => state.user.loginInfo

}

export default getters