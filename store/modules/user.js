import Vue from 'vue'
import {
	postAPI,
	getAPI
} from "@/utils/api.js"
import {
	config
} from "@/utils/config.js"
export default {
	namespaced: true,
	state: {
		token: '',
		accessToken:'',
		loginInfo: {},
	},
	mutations: {
		SET_TOKEN:(state,token)=>{
			uni.setStorageSync('token',token);
			state.token = token;
		},
		SET_USER_INFO:(state,data)=>{
			state.loginInfo = data;
		},
		SET_BAI_TOKEN:(state,token)=>{
			state.accessToken = token;
		}
	},

	actions: {
		login: (state, params) => {
			return new Promise((resolve, reject) => {
				postAPI(config.loginMoblie,params).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		getLoginUserState: (state) => {
			return new Promise((resolve, reject) => {
				getAPI(config.getAllUserState).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		getBAIToken:()=>{
			return new Promise((resolve, reject) => {
				var data = {
				   grant_type:'client_credentials',
				   client_id:'in2qtswuORpp9DhOfyLKrMnG',
				   client_secret:'bi5hd5tmk8fkQnQtUaOPf1C3MlSZMIHt'
					//client_id:'uanBrruibVxCOf9cqOMpu0ff',
					//client_secret:'YCp4Ii1DVqTC1oFxKonIRKhK7Oypjpq6'
				}
				uni.request({
					url: 'https://aip.baidubce.com/oauth/2.0/token',
					method: 'POST',
					data: data,
					header: {
						'content-type': 'application/x-www-form-urlencoded',
					},
					success(res) {
						resolve(res.data.access_token)
					}
				})
			})
		}
	}
}
