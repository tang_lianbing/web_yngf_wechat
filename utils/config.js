export const imgHost = 'https://public-base.oss-cn-chengdu.aliyuncs.com/agile/';
export const imgsHost = 'https://pro-lhyg.oss-cn-shenzhen.aliyuncs.com/cloud/web/';
export const signFile = 'https://single-cq.oss-cn-beijing.aliyuncs.com/';
export const imgBankHost = 'https://jht-cq.oss-cn-chengdu.aliyuncs.com/wechat/';
export const fileUrl = 'https://www.taxcha.cn/pdf-app/web/viewer.html';

// const host = 'http://192.168.0.104:8083'; // 本地环境
const host = 'https://wsftest.yinnengyun.com'; // 测试环境
// const host = 'https://wechat.taxdata.top'; // 正式环境


export const config = {
  loginMoblie: `${host}/wx/user/login`,
	getPhoneNumber: `${host}/wx/user/getPhoneNumber`,
	getAllUserState:`${host}/wx/user/selectUserStateByUserId`, 
	userThirdAuth: `${host}/wx/user/auth`,
	userThirdFace: `${host}/wx/user/faceDetection`,
	userSeaOpenAccount: `${host}/wx/user/visAcctOpen`,
	userEducation: `${host}/wx/info/completeEducation`,
	userWork: `${host}/wx/info/completeWorkExperience`,
	getUserEducationWork:`${host}/wx/user/selectLoginExtraInfo`,
	getUserRecords: `${host}/wx/record/pageQueryUserTransactionRecords`,
	
	getUserSignList: `${host}/wx/user/listSigns`,
	getSignDetail:`${host}/wx/qr/addContract`,
	submitSign: `${host}/wx/user/sign`,
	getSmsSwitch: `${host}//wx/common/switch`, // 获取发短信开关
	getSendSmsCode: `${host}/wx/common/V2/sendSmsCode`, // 新增发送短信
	verifyThreeElements: `${host}/wx/user/verifyThreeElements`, // 三要素验证
	pdfConvertPng: `${host}/wx/common/pdfConvertPng`, // pdf文件转图片
	
	baseUploadFile: `${host}/wx/common/uploadFile`,
	getBaseSmsCode: `${host}/wx/common/sendSmsCode`,
	
	userSeaSettlement: `${host}/wx/user/visTransferOut`,
	userSeaBindingBank: `${host}/wx/user/visAccUserBinding`,
	getuserBank: `${host}/wx/user/visAccBinding`,
	getuserBalance: `${host}/wx/user/visAcctBalance`,
	getBaseSeaMoneySmsCode: `${host}/wx/common/visGetOTP`,
	baseSeaOpenAccountSmsCode: `${host}/wx/common/visAcctOpenCode`,
	getBaseSeaBindingBankSmsCode: `${host}/wx/common/visAcctOpenCheck`,
	
	getTaskList: `${host}/wx/task/pageQueryTask`,
	getTaskDetail: `${host}/wx/task/selectTaskDetail`,
	getUserTaskEnrollDetail: `${host}/wx/task/selectTaskRegisDetail`,
	getUserTaskSignState: `${host}/wx/task/selectUserPactSign`,
	taskBack: `${host}/wx/task/quitTask`,
	taskEnroll: `${host}/wx/task/taskRegistration`,
	isTaskEnroll:`${host}/wx/task/taskReRegistration`,
	getMyTaskList: `${host}/wx/task/pageQueryMyTasks`,
	submitTaskDeliver: `${host}/wx/task/submitTaskDelivery`,
	getUserTaskDeliver: `${host}/wx/task/selectTaskDelivery`,
	getUserTaskDeliverSettle: `${host}/wx/task/pageQueryTaskImportSettlement`,
	getUserTaskSettle: `${host}/wx/task/pageQueryTaskSettlement`,
};


