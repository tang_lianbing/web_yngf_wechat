import store from '../store/index.js'
var util = require('./util')
import {
	pathToBase64
} from './image-tools.js'
/**
 * pathToBase64  uni-app 插件里面图片转base64的方法
 * OcrIdCard()  判断实名信息的方法
 * access_token  百度认证的token，多数是后台从应用里面获取ak sk
 * forntOrBack  判断图片上传正面或背面
 * */
/* function OcrIdCard(access_token, frontOrBack) {
	return new Promise(function(resolve, reject) {
		var that = this;
		//识别身份证
		uni.chooseImage({
			count: 1,
			sizeType: ['compressed'],
			sourceType: ['album', 'camera'],
			success: function(res) {
				// console.log(res.tempFilePaths)
				pathToBase64(res.tempFilePaths[0])
					.then(base64 => {
						let afterbase = base64.replace(/^data:image\/\w+;base64,/, '')
						// console.log(afterbase);
						// 返回正面图片上传数据
						uni.showLoading({
							title: '识别中'
						})
						uni.request({
							url: 'https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token=' + access_token,
							method: 'POST',
							header: {
								'Content-Type': 'application/x-www-form-urlencoded'
							},
							data: {
								image: afterbase,
								id_card_side: frontOrBack
							},
							success(_res) {
								uni.hideLoading();
								resolve({
									info: _res,
									img: base64,
									imageUpdate: afterbase
								})
							},
							fail(_res) {
								uni.hideLoading();
								uni.showToast({
									title: '请求出错',
								})
								reject(_res)
							}
						})
					})
					.catch(error => {
						console.error(error)
					})
			}
		})
	})
} */

const OcrIdCard = (frontOrBack,ocrOrType,$loading) => {
	return new Promise(function(resolve, reject) {
		var that = this;
		uni.chooseImage({
		    count: 1,
		    success: function(res) {
				try {
					uni.serviceMarket.invokeService({
						service: 'wx79ac3de8be320b71',
						api: 'OcrAllInOne',
						data: {
						  img_url: new uni.serviceMarket.CDN({
							type: 'filePath',
							filePath: res.tempFilePaths[0],
						  }),
						  data_type: 3,
						  ocr_type: ocrOrType
						},
					}).then(req=>{
						$loading = false;
						if(req.errMsg === "invokeService:ok"){
							if(req.data.idcard_res.type ===1 && frontOrBack === 'imgOcrT'){
								util.showBusy('请上传身份证正面图片！');
							}else if(req.data.idcard_res.type ===2 && frontOrBack === 'imgOcrF')
							{
								util.showBusy('请上传身份证反面图片！');
							}else{
								resolve({
									file: res.tempFilePaths[0]
								})
							}
						}
					}).catch((err)=>{
						util.showBusy('请上传有效正面身份证！');
						$loading = false;
					})
				} catch (err) {
					$loading = false;
					console.log(err);
				}
		    },fail: function(res) {}
		})
	})
}

module.exports = {
	OcrIdCard: OcrIdCard
}
