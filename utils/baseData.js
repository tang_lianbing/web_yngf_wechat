export const SIGN_STATE_DATA = [
   // { value: 1, label: '静默签(不发短信)' },
    { value: 2, label: '短信签约' },
    { value: 3, label: '小程序签约' },
    { value: 4, label: '线下签约' }
]

export const taskConfig = [
    { name: '任务名称', key: 'fieldTaskName', type: 'text' },
    { name: '园区服务商', key: 'fieldTaskPark', type: 'select' },
    { name: '任务类型', key: 'fieldTaskType', type: 'select' },
    { name: '任务区域', key: 'fieldTaskRegion', type: 'select' },
    { name: '需要人数', key: 'fieldTaskPeopleNumber', type: 'text' },
    { name: '执行开始日期', key: 'fieldTaskStartDate', type: 'date' },//singleDate doubleDate
    { name: '执行结束日期', key: 'fieldTaskEndDate', type: 'date' },
    { name: '任务价格', key: 'fieldTaskPrice', type: 'text' },
    { name: '需求描述', key: 'fieldTaskDescription', type: 'textarea' },
    { name: '报名要求', key: 'fieldTaskEnroll', type: 'textarea' },
    { name: '交付要求', key: 'fieldTaskDeliver', type: 'textarea' }
]

export const TASK_CATEGORY_DATA = [[
	{ value: '', label: '全部' },
    { value: '1', label: '房地产服务' },
    { value: '2', label: '信息技术服务' },
    { value: '3', label: '教育服务' },
    { value: '4', label: '文化传媒服务' },
    { value: '5', label: '物流服务' },
		{ value: '6', label: '现代服务' },
    { value: '7', label: '其他服务' }
]]

export const ORDER_DATA = [
	[
		{ value: 1, label: '倒序' },
		{ value: 2, label: '升序' }
	]
]

export const TASK_STATE_DATA = [
	{ value: -1, label: '可报名'},
    { value: 1, label: '已报名'},
    { value: 2, label: '已中标'},
    { value: 3, label: '未中标'},
    { value: 4, label: '已取消'},
    { value: 5, label: '待验收'},
    { value: 6, label: '已完成'},
	{ value: 7, label: '验收失败'},
	{ value: 8, label: '已过期'}
]

export const STLLE_TYPE_DATA = [[
	{ value: '', label: '全部'},
	{ value: 1, label: '结算'},
	{ value: 2, label: '提现'},
	/* { value: 3, label: '充值'},
	{ value: 4, label: '消费'} */
]]

export const TASK_STLLE_STATE_DATA = [
	{ value: 1, label: '待审核'},
	{ value: 2, label: '结算中'},
	{ value: 3, label: '结算成功'},
	{ value: 4, label: '部分结算成功'},
	{ value: 5, label: '结算失败'},
	{ value: 6, label: '已驳回'}
]

export const CITY_DATA = [[
	{
		"label": "全部",
		"value": "0"
	},
    {
        "label": "北京市",
        "value": "11"
    },
    {
        "label": "天津市",
                "value": "12"
    },
    {
        "label": "河北省",
                "value": "13"
    },
    {
        "label": "山西省",
                "value": "14"
    },
    {
         "label": "内蒙古自治区",
                "value": "15"
    },
    {
        "label": "辽宁省",
                "value": "21"
    },
    {
        "label": "吉林省",
                "value": "22"
    },
    {
        "label": "黑龙江省",
                "value": "23"
    },
    {
        "label": "上海市",
                "value": "31"
    },
    {
        "label": "江苏省",
                "value": "32"
    },
    {
        "label": "浙江省",
                "value": "33"    
    },
    {
        "label": "安徽省",
                "value": "34"
    },
    {
        "label": "福建省",
                "value": "35"
    },
    {
        "label": "江西省",
                "value": "36"
    },
    {
        "label": "山东省",
                "value": "37"
    },
    {
        "label": "河南省",
                "value": "41"
    },
    {
        "label": "湖北省",
                "value": "42"
    },
    {
         "label": "湖南省",
                "value": "43"
    },
    {
        "label": "广东省",
                "value": "44"
    },
    {
        "label": "广西壮族自治区",
                "value": "45"
    },
    {
         "label": "海南省",
                "value": "46"
    },
    {
        "label": "重庆市",
                "value": "50"
    },
    {
        "label": "四川省",
                "value": "51"
    },
    {
        "label": "贵州省",
                "value": "52"
    },
    {
        "label": "云南省",
                "value": "53"
    },
    {
        "label": "西藏自治区",
                "value": "54"
    },
    {
        "label": "陕西省",
                "value": "61"
    },
    {
         "label": "甘肃省",
                "value": "62"
    },
    {
        "label": "青海省",
                "value": "63"
    },
    {
        "label": "宁夏回族自治区",
                "value": "64"
    },
    {
        "label": "新疆维吾尔自治区",
                "value": "65"
    },
    {
        "label": "香港特别行政区",
                "value": "81"
    },
    {
         "label": "澳门特别行政区",
                "value": "82"
    },
    {
        "label": "台湾省",
                "value": "71"
    }
]]