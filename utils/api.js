var util = require('./util')
export function postAPI(url, params) {
	const token = uni.getStorageSync('token');
	let header = {
		"Content-type": "application/json",
		'Accept': 'application/json'
	};
	if (token && token !== '') {
		header['accessToken'] = token;
	}
	return new Promise((resolve, reject) => {
		if (params.isTrue && !params.isTrue) {
			uni.showLoading({
				title: '加载中...'
			});
		}
		uni.request({
			url: url,
			data: params,
			header: header,
			method: "POST",
			success: (response) => {
				uni.hideLoading();
				if (response.data.code === '106') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else if (response.data.code === '103') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else if (response.data.code === '500') {
					resolve(response.data)
					util.showBusy(response.data.message);
				} else {
					resolve(response.data)
				}
			},
			fail: (error) => {
				resolve()
				uni.hideLoading();
				if (error && error.response) {
					showError(error.response);
				}
			},
		});
	})
}

export function getAPI(url, params) {
	const token = uni.getStorageSync('token');
	let header = {
		"Content-Type": "application/json",
		'Accept': 'application/json'
	};
	if (token && token !== '') {
		header['accessToken'] = token;
	}
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '加载中...'
		});
		uni.request({
			url: url,
			data: params,
			header: header,
			method: "GET",
			success: (response) => {
				uni.hideLoading();
				if (response.data.code === '106') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else if (response.data.code === '103') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else {
					resolve(response.data)
				}
			},
			fail: (error) => {
				resolve();
				uni.hideLoading();
				if (error && error.response) {
					showError(error.response);
				}
			}
		});
	})
}

export function putAPI(url, params) {
	const token = uni.getStorageSync('token');
	let header = {
		"Content-type": "application/x-www-form-urlencoded",
		'Accept': 'application/json'
	};
	if (token && token !== '') {
		header['accessToken'] = token;
	}
	return new Promise((resolve, reject) => {
		uni.showLoading({
			title: '加载中...'
		});
		uni.request({
			url: url,
			data: params,
			header: header,
			method: "GET",
			success: (response) => {
				uni.hideLoading();
				if (response.data.code === '106') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else if (response.data.code === '103') {
					util.showBusy('登录过期，请重新登录');
					setTimeout(function() {
						wx.navigateTo({
							url: '/pages/login/login'
						})
					}, 2000)
				} else {
					resolve(response.data)
				}
			},
			fail: (error) => {
				resolve();
				uni.hideLoading();
				if (error && error.response) {
					//reject(error.response)
					showError(error.response);
				}
			}
		});
	})

}

const showError = error => {
	uni.showToast({
		title: error.message,
		icon: 'none',
		duration: 3000,
		complete: function() {
			setTimeout(function() {
				uni.hideToast();
			}, 3000);
		}
	});
}